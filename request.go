package reqp

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"github.com/ttacon/libphonenumber"
)

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

type ContentsDataType int

const (
	// POST request containing a json payload
	JSON ContentsDataType = iota

	// POST request containing url form encoded values
	FormURLEncoded

	// GET request containing url params
	URLParams
)

type DataType int

func (ft DataType) String() string {
	return map[DataType]string{
		String: "string",
		Number: "number",
		Bool:   "bool",
		Array:  "array",
		Object: "Object",
	}[ft]
}

const (
	String DataType = iota
	Number
	Bool
	Array
	Object
)

type DataDoc struct {
	Info string
}

func (doc DataDoc) Header() http.Header {
	return http.Header{}
}

func (doc DataDoc) Write(data []byte) (int, error) {
	doc.Info = string(data)
	return 0, nil
}

func (doc DataDoc) WriteHeader(statusCode int) {}

func (doc DataDoc) Spec() interface{} {
	return nil
}

type Def struct {
	Name      string
	Desc      string
	Type      DataType
	IsArray   bool
	ArrayType DataType
	Options   []Option
}

func (def *Def) Optional() bool {

	for _, option := range def.Options {
		if _, is := option.(*OptionOptional); is {
			return true
		}
	}

	return false
}

type Option interface {
	Validate(v interface{}) error
}

type OptionOptional struct{}

func (o OptionOptional) Validate(v interface{}) error { return nil }

type GreaterThanOption struct {
	v float64
}

func (o GreaterThanOption) Validate(v interface{}) error {

	i, is := v.(float64)

	if !is {
		return errors.New("not a number")
	}

	if i < o.v {
		if o.v == float64(int64(o.v)) {
			return errors.Errorf("lower than %d", int64(o.v))
		} else {
			return errors.Errorf("lower than %f", o.v)
		}
	}

	return nil
}

type EmailValidator struct{}

func (o EmailValidator) Validate(v interface{}) error {
	if !emailRegex.MatchString(v.(string)) {
		return errors.Errorf("not an email")
	}
	return nil
}

type PhoneValidator struct{}

func (o PhoneValidator) Validate(v interface{}) error {
	_, err := libphonenumber.Parse(v.(string), "RO")
	if err != nil {
		return err
	}
	return nil
}

func Optional() Option {
	return &OptionOptional{}
}

func GreaterThan(than float64) Option {
	return &GreaterThanOption{than}
}

func Email() Option {
	return &EmailValidator{}
}

func Phone() Option {
	return &PhoneValidator{}
}

type RequestData struct {
	w http.ResponseWriter
	r *http.Request
	t ContentsDataType

	keys map[string]*Def

	data map[string]interface{}

	verified bool
}

func New(w http.ResponseWriter, r *http.Request, t ContentsDataType) *RequestData {
	return &RequestData{
		w:    w,
		r:    r,
		t:    t,
		keys: make(map[string]*Def),
		data: make(map[string]interface{}),
	}
}

func (dat *RequestData) need(key string, typ DataType, desc string, opt []Option) *RequestData {
	dat.keys[key] = &Def{Name: key, Desc: desc, Type: typ, Options: opt}
	return dat
}

func (dat *RequestData) NeedArray(key string, desc string, typ DataType, opt ...Option) *RequestData {
	dat.need(key, Array, desc, opt)
	dat.keys[key].IsArray = true
	dat.keys[key].ArrayType = typ
	return dat
}

func (dat *RequestData) NeedBool(key string, desc string, opt ...Option) *RequestData {
	dat.need(key, Bool, desc, opt)
	return dat
}

func (dat *RequestData) NeedString(key string, desc string, opt ...Option) *RequestData {
	dat.need(key, String, desc, opt)
	return dat
}

func (dat *RequestData) NeedNumber(key string, desc string, opt ...Option) *RequestData {
	dat.need(key, Number, desc, opt)
	return dat
}

func (dat *RequestData) HasKey(key string) bool {
	_, has := dat.data[key]
	return has
}

func (dat *RequestData) get(key string) interface{} {

	dat.Verify()

	v, exist := dat.data[key]

	if !exist {
		panic(fmt.Sprintf("Key %s is missing", key))
	}

	return v
}

func (dat *RequestData) GetStringArray(key string) []string {
	out := make([]string, 0)
	for _, item := range dat.get(key).([]interface{}) {
		out = append(out, item.(string))
	}
	return out
}

func (dat *RequestData) GetString(key string) string {
	v, _ := dat.data[key]
	return v.(string)
}

func (dat *RequestData) GetBool(key string) bool {
	v, _ := dat.data[key]
	return v.(bool)
}

func (dat *RequestData) GetNumber(key string) float64 {
	v, _ := dat.data[key]
	return v.(float64)
}

func (dat *RequestData) GetTime(key string) time.Time {
	return time.Now()
}

func (dat *RequestData) Verify() error {

	if dat.verified {
		return nil
	}

	dat.verified = true

	if dat.r == nil {
		return errors.New("Request body is empty")
	}

	if dat.t == JSON {

		data, err := ioutil.ReadAll(dat.r.Body)

		if err != nil {
			return errors.Wrap(err, "Failed to read body")
		}

		err = json.Unmarshal(data, &dat.data)
		if err != nil {
			return errors.Wrap(err, "Failed to decode json from body")
		}
	}

	parseUrlValues := func(values url.Values) {

		for key, value := range values {

			if len(value) > 1 {
				// array
				dat.data[key] = value
				continue
			}

			// verify if query param is number
			if i, err := strconv.Atoi(value[0]); err == nil {
				dat.data[key] = float64(i)
			} else {
				dat.data[key] = value[0]
			}
		}
	}

	if dat.t == FormURLEncoded {

		data, err := ioutil.ReadAll(dat.r.Body)

		if err != nil {
			return errors.Wrap(err, "Failed to read body")
		}

		values, err := url.ParseQuery(string(data))
		if err != nil {
			return err
		}
		parseUrlValues(values)

	}

	if dat.t == URLParams {
		parseUrlValues(dat.r.URL.Query())
	}

	for key, def := range dat.keys {

		value, exist := dat.data[key]

		if !exist && !def.Optional() {
			return errors.Errorf(
				"Field %s is required",
				key,
			)
		}

		if exist {
			for _, option := range def.Options {
				if err := option.Validate(value); err != nil {
					return errors.Wrap(err, fmt.Sprintf(
						"Field value of %s is invalid",
						key,
					))
				}
			}
		}

		if err := validate(value, def.Name, def.Type); exist && err != nil {
			return err
		}
	}

	return nil
}

func (dat *RequestData) PrintDoc(w http.ResponseWriter) {

	buf := &bytes.Buffer{}

	for key, def := range dat.keys {

		if def.IsArray {

			buf.WriteString(
				fmt.Sprintf(
					"%s: []%s - %s",
					key,
					def.ArrayType.String(),
					def.Desc,
				),
			)
		} else {

			buf.WriteString(
				fmt.Sprintf(
					"%s: %s - %s",
					key,
					def.Type.String(),
					def.Desc,
				),
			)
		}

		if def.Optional() {
			buf.WriteString(" ( optional )\n")
		}

	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(buf.Bytes())
}

func validate(o interface{}, name string, t DataType) error {

	v := reflect.ValueOf(o)

	mapType := map[DataType]reflect.Kind{
		String: reflect.String,
		Number: reflect.Float64,
	}

	kind, exist := mapType[t]

	if exist && v.Kind() != kind {
		return errors.Errorf(
			"Field %s expected to be %s",
			name,
			t.String(),
		)
	}

	if t == String && v.IsZero() {
		return errors.Errorf("Field %s is empty", name)
	}

	return nil
}

func GetHandlerDoc(handler http.HandlerFunc) (doc *DataDoc) {

	defer func() { recover() }()

	doc = &DataDoc{}

	handler(doc, nil)

	return doc
}
