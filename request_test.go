package reqp

import (
	"testing"
)

func TestRequestData(t *testing.T) {
	doc := &DataDoc{}
	dat := New(doc, nil, JSON)
	dat.NeedNumber("a", "a desc")
	dat.Verify()
	doc.Spec()
}
